import User from "@/models/User";
import {ROLES} from "@/plugins/CONSTANTS";

export default {
  namespaced: true,
  state: () => ({
    user: null,
    ready: false
  }),
  getters: {
    get: (s) => s.user,
    depId: (s) => s.user?.depId,
    id: (s) => s.user?.id,
    role: (s) => s.user?.role,
    email: (s) => s.user?.email,
    isReady: (s) => s.ready,
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true
      s.user = new User(v)
    },
    clear: (s) => s.user = null
  },
  actions: {
    onLogOut({commit}) {
      commit('clear')
    },
  }
}