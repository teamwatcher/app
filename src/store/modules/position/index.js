import Position from "@/models/Position";

export default {
  namespaced: true,
  state: () => ({
    positions: null,
    ready: false
  }),
  getters: {
    get: (s) => s.positions,
    getByDepId: (s) => (id) => s.positions.filter(item => item.depId === id),
    getById: (s) => (id) => s.positions.find(p => p.id === id),
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.positions = v.map(item => new Position(item))
    },
    clear: (s) => s.positions = null
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({ dispatch}) {
      const path = '/api/position'
      const setter = 'position/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}