import User from "@/models/User";
import {defineAbilitiesFor} from "@/plugins/ability";

export default {
  namespaced: true,
  state: {
    appName: 'TeamWatcher.ru',
    accessLevel: 0,
    ready: false,
  },
  getters: {
    isReady: (s) => s.ready,
    getAppName: (s) => s.appName,
    getAccessLevel: (s) => s.accessLevel,
  },
  mutations: {
    setAccessLevel: (s, user) => {
      if (!user) {
        return s.accessLevel = 0
      }

      switch (user.status) {
        case User.STATUSES.CREATED:
          s.accessLevel = 1;
          break;
        case User.STATUSES.INVITED:
          s.accessLevel = 1;
          break;
        case User.STATUSES.ACTIVE:
          s.accessLevel = 2;
          break;
      }
    },
    setReady: (s, v) => s.ready = v,
    clearData: (s) => {
      s.accessLevel = 0
      s.ready = false
      s.orgId = null
    }
  },
  actions: {
    onLogOut({commit}) {
      commit('clearData')
    },
    onAuthStateChanged({getters, commit, dispatch}, user) {
      commit('setReady', false)

      if (user) {
        dispatch('loadAppWithUser', user)
      } else {
        if (getters.isLoaded) dispatch('clearModulesData', {root: true})
        dispatch('loadAppWithoutUser')
      }
    },
    async loadAppWithUser({commit, dispatch}, user) {
      if (user.isActive) await dispatch('loadUserData', user)

      commit('setReady', true)
    },
    loadAppWithoutUser({commit}) {
      commit('setAccessLevel', 0)
      commit('setReady', true)
    },
    async loadUserData({commit, dispatch}, {orgId}) {
      await dispatch('organization/updateData', orgId, {root: true})
      await dispatch('initModules', {}, {root: true})
      dispatch('defineAbility')
    },
    defineAbility({rootGetters}) {
      const user = rootGetters['currentUser/get']
      const {rootDep} = rootGetters['organization/get']

      defineAbilitiesFor(user, rootDep.id)
    }
  }
}
