import User from "@/models/User";

export default {
  namespaced: true,
  state: () => ({
    users: [],
    usersMap: {},
    ready: false
  }),
  getters: {
    get: (s) => s.users?.sort((a, b) => a.role > b.role ? -1 : 1)  || [],
    getMap: (s) => s.usersMap || {},

    getUserById: (s) => (id) => s.usersMap[id],
    getDisplayNameByID: (s) => (id) => s.usersMap[id]?.displayName,
    getUsersByDep: (s) => (id) => s.users.filter(user => user.depId === id),
    getUsersByTeam: (s) => (id) => s.users.filter(user => user.job.team === id),
    getUsersByTask: (s) => (id) => s.users.filter(user => user.job.taskList.includes(id)),

    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.users = v.map(user => {
        const u = new User(user)
        s.usersMap[u.id] = u
        return u
      })
    },
    clear: (s) => {
      s.users = null
    }
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({ commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/user'
      const setter = 'users/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  }
}