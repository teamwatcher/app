import Department from "@/models/Department";

export default {
  namespaced: true,
  state() {
    return {
      deps: [],
    }
  },
  getters: {
    get: (s) => s.deps,
    getById: (s) => (id) => s.deps.find(team => team.id === id),
  },
  mutations: {
    set: (s, v) => {
      s.deps = v.map(item => new Department(item))
    },
    clear: (s) => s.deps = []
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({ commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/dep'
      const setter = 'dep/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}