export default {
  namespaced: true,
  state: () => ({
    items: []
  }),
  getters: {
    get: (s) => s.items,
  },
  mutations: {
    add: (s, item) => {
      item.id = 'bc' + Date.now()
      s.items.push(item)
    },
    remove: (s, id) => {
      s.items = s.items.filter(item => item.id !== id)
    },
    clear: (s) => s.items = []
  }
}