import Team from "@/models/Team";

export default {
  namespaced: true,
  state() {
    return {
      teams: [],
      ready: false
    }
  },
  getters: {
    get: (s) => s.teams,
    getByDepId: (s) => (id) => s.teams.filter(team => team.depId === id),
    getById: (s) => (id) => s.teams.find(team => team.id === id),
    getTitleById: (s) => (id) => s.teams.find(team => team.id === id)?.title,
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.teams = v.map(item => new Team(item))
    },
    clear: (s) => s.teams = []
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({ commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/team'
      const setter = 'team/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}