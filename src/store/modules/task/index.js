import Task from "@/models/Task";

export default {
  namespaced: true,
  state: () => ({
    tasks: [],
    ready: false
  }),
  getters: {
    get: (s) => s.tasks,
    getByDepId: (s) => (id) => s.tasks.filter(item => item.depId === id),
    getById: (s) => id => s.tasks.find(task => task.id === id),
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) {
        s.ready = true
      }
      s.tasks = v.map(task => new Task(task))
    },
    clear: (s) => s.tasks = null
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/task'
      const setter = 'task/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}