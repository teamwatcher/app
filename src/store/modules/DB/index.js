import $api from "@/services/ApiSetupService";

export default {
  namespaced: true,
  actions: {
    async updateData({commit, dispatch}, {path, setter, dispatcher}) {
      try {
        const {data} = await $api.get(path)

        if (setter) commit(setter, data, {root: true})
        if (dispatcher) dispatch(dispatcher, data, {root: true})
      }catch (e) {

      }
    },
  }
}