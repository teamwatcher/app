
export default {
  namespaced: true,
  state: () => ({
    email: "",
    isAuth: false
  }),
  getters: {
    email: (s) => s.email,
    isAuth: (s) => s.isAuth,
  },
  mutations: {
    setEmail: (s, v) => s.email = v,
    setAuth: (s, v) => s.isAuth = v,
  }
}