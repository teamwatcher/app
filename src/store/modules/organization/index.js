import Organization from "@/models/Organization";

export default {
  namespaced: true,
  state: () => ({
    organization: null,
    ready: false
  }),
  getters: {
    getId: (s) => s.organization.id,
    get: (s) => s.organization || {},
    getRootDep: (s) => s.organization?.rootDep,
    getRootDepOwner: (s) => s.organization?.rootDep?.ownerId.id,
    permission: (s) => s.organization ? s.organization.permission : {},
    isReady: (s) => s.ready,
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true
      s.organization = new Organization(v)
    },
    clear: (s) => {
      s.id = null
      s.organization = null
    }
  },
  actions: {
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({getters, dispatch}, orgId) {
      if (!orgId) orgId = getters['getId']
      const path = '/api/org/get/' + orgId

      const setter = 'organization/set'
      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}