import Vue from 'vue'
import Vuex from 'vuex'

import breadcrumbs from "./modules/breadcrumbs";
import app from './modules/app'
import message from './message'
import DB from './modules/DB'
import auth from './modules/auth'
import organization from './modules/organization'
import dep from './modules/dep'
import currentUser from './modules/currentUser'
import users from './modules/users'
import team from './modules/team'
import position from './modules/position'
import task from './modules/task'
import planner from '@/_modules/planner/store'
import performance from '@/_modules/performance/store'

const modules = {
  breadcrumbs,
  message,
  DB,
  organization,
  currentUser,
  dep,
  users,
  team,
  position,
  task,
  auth,
  app,
  planner,
  performance
}

Vue.use(Vuex)

function dispatchInEachModules(dispatch, modules, action, parent = '') {
  const promises = []
  for (let name in modules) {
    const module = modules[name]
    try {
      if (module.actions && module.actions[action]) {
        promises.push(dispatch(`${parent}${name}/${action}`))
      }

      if (module.modules) {
        promises.push(...dispatchInEachModules(dispatch, module.modules, action, parent + name + '/'))
      }
    } catch (e) {

    }
  }

  return promises
}

export default new Vuex.Store({
  state: {
    sandboxMode: false
  },
  getters: {
    sandboxMode: (s) => s.sandboxMode
  },
  mutations: {
    setSandboxMode: (s, v) => s.sandboxMode = v
  },
  actions: {
    async initModules({dispatch}) {
      return Promise.all(dispatchInEachModules(dispatch, modules, 'initialize'))
    },
    clearModulesData({dispatch}) {
      return Promise.all(dispatchInEachModules(dispatch, modules, 'onLogOut'))
    },
    sandboxModeOn({commit}) {
      commit('setSandboxMode', true)
    },
    sandboxModeOff({commit}) {
      commit('setSandboxMode', false)
    }
  },
  modules
})
