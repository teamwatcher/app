import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import {plannerRoutes, plannerSettingsRoutes} from '@/_modules/planner'
import {performanceRoutes} from '@/_modules/performance'

import {authRoutes} from "@/router/auth";
import {addModuleName, getNormalModuleRoutes} from "@/router/utils";

const Home = () => import('@/views/App/Home')
const Employee = () => import('@/views/App/Employee')

const Organization = () => import('@/views/App/Organization')
const OrganizationEmployees = () => import('@/views/App/OrganizationEmployees')
const OrganizationStructure = () => import('@/views/App/OrganizationStructure')

const Setting = () => import('@/views/App/Setting/Setting')
const SettingProfile = () => import('@/views/App/Setting/SettingProfile')
const SettingAccessPermission = () => import('@/views/App/Setting/SettingAccessPermission')
const SettingOrg = () => import('@/views/App/Setting/SettingOrg')


Vue.use(VueRouter)


const commonRoutes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      layout: 'LayoutApp',
        title: 'Главная',
        protected: {
        accessLevel: [2]
      }
    }
  },
  {
    path: '/organization',
    name: 'Organization',
    component: Organization,
    redirect: {name: 'OrganizationEmployees'},
    meta: {
      layout: 'LayoutApp',
      title: 'Организация',
      breadcrumb: "Организация",
      protected: {
        accessLevel: [2]
      }
    },
    children: [
      {
        path: 'employees',
        name: 'OrganizationEmployees',
        component: OrganizationEmployees,
        meta: {
          title: "Сотрудники",
          breadcrumb: "Сотрудники",
          layout: 'LayoutApp',
          protected: {
            accessLevel: [2]
          }
        }
      },
      {
        path: 'structure',
        name: 'OrganizationStructure',
        component: OrganizationStructure,
        meta: {
          breadcrumb: "Структура",
          title: "Структура",
          layout: 'LayoutApp',
          protected: {
            accessLevel: [2]
          }
        }
      }
    ]
  },
  {
    path: '/organization/employees/:uid',
    name: 'Employee',
    component: Employee,
    meta: {
      layout: 'LayoutApp',
      title: 'Профиль',
      protected: {
        accessLevel: [2]
      },
      breadcrumb: (routeParams) => {
        const userDisplayName = store.getters['users/getDisplayNameByID'](routeParams.uid)

        return {
          label: userDisplayName, parent: 'OrganizationEmployees'
        }
      }
    }
  },
  {
    path: '/settings',
    name: 'Setting',
    component: Setting,
    redirect: {name: 'SettingProfile'},
    meta: {
      breadcrumb: "hidden", layout: 'LayoutApp', title: 'Настройки', protected: {
        accessLevel: [2]
      },
    },
    children: [
      {
        path: 'profile',
        name: 'SettingProfile',
        component: SettingProfile,
        meta: {
          breadcrumb: "hidden",
          layout: 'LayoutApp',
          title: 'Настройки',
          protected: {
            accessLevel: [2]
          },
        }
      },
      {
        path: 'organization',
        name: 'SettingOrg',
        component: SettingOrg,
        meta: {
          breadcrumb: "hidden",
          layout: 'LayoutApp',
          title: 'Настройки',
          protected: {
            accessLevel: [2]
          },
        }
      },
      // {
      //   path: 'access-permission',
      //   name: 'SettingAccessPermission',
      //   component: SettingAccessPermission,
      //   meta: {
      //     breadcrumb: "hidden",
      //     layout: 'LayoutApp',
      //     title: 'Настройки',
      //     protected: {
      //       accessLevel: [2]
      //     },
      //   }
      // },
      ...plannerSettingsRoutes
    ]
  },
]

const routes = [
  ...commonRoutes,
  ...getNormalModuleRoutes(authRoutes, 'auth'),
  ...plannerRoutes,
  ...performanceRoutes
]

const router = new VueRouter({
  mode: "history", routes
})

router.beforeEach(async (to, from, next) => {
  const accessLevel = store.getters['app/getAccessLevel']

  if (!to.meta.protected) {
    next()
  } else {
    const accessLevels = to.meta.protected.accessLevel
    if (accessLevels.some(a => a === accessLevel)) {
      next()
    } else {
      switch (store.getters["auth/isAuth"]) {
        case false:
          next({name: 'Login'});
          break;
        case true:
          next({name: 'Home'});
          break;
      }
    }
  }
})

export default router
