const Login = () => import('@/views/Auth/Login.vue')
const Register = () => import('@/views/Auth/RegistrationByEmail.vue')
const RegisterLink = () => import('@/views/Auth/RegistrationByLink.vue')
const ForgetPassword = () => import('@/views/Auth/ResetPassword.vue')
const EmailSending = () => import('@/views/Auth/EmailSending')

export const authRoutes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      layout: 'LayoutAuth',
      title: 'Авторизация',
      protected: {
        accessLevel: [0, 1]
      }
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      layout: 'LayoutAuth',
      title: 'Регистрация',
      protected: {
        accessLevel: [0, 1]
      }
    }
  },
  {
    path: '/register/link',
    name: 'RegisterLink',
    component: RegisterLink,
    meta: {
      layout: 'LayoutEmpty',
      title: 'Регистрация',
      protected: {
        accessLevel: [0, 1]
      }
    }
  },
  {
    path: '/resetpassword',
    name: 'Reset',
    component: ForgetPassword,
    meta: {
      layout: 'LayoutAuth',
      title: 'Восстановление пароля',
      protected: {
        accessLevel: [0, 1]
      }
    }
  },
  {
    path: '/emailsending',
    name: 'EmailSending',
    component: EmailSending,
    meta: {
      layout: 'LayoutAuth',
      title: 'Запрос отправлен',
      protected: {
        accessLevel: [0, 1]
      }
    }
  }
]