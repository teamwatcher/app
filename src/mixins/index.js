import {mapGetters} from "vuex";
import Tools from "@/plugins/Tools";
import Team from "@/models/Team";

export const currentUserId = {
  computed: {
    ...mapGetters("currentUser", {currentUserId: "id"}),
  }
}

export const users = {
  computed: {
    users() {
      const users = this.$store.getters['users/get']

      return users.filter(u => this.$can('read', u))
    }
  }
}

export const departments = {
  computed: {
    departments() {
      return this.$store.getters["dep/get"]
    }
  }
}

export const updateQuery = {
  methods: {
    updateQuery(key, val) {
      const route = this.$route
      const path = route.path
      const query = Object.assign({}, route.query, {})

      if (!val) {
        if (query.hasOwnProperty(key)) {
          delete query[key]
        }
      } else {
        query[key] = val
      }

      if (!Tools.isEqual(query, route.query)) {
        this.$router.replace({path, query})
      }
    },
  }
}

export const CUSTOM_FILTER = {
  methods: {
    CUSTOM_FILTER(value, search, item) {
      const teamFilterKey = 'job.team'
      const filters = JSON.parse(search || "")

      let filterKeys = Object.keys(filters).filter(f => filters[f] !== null)
      if (!filterKeys.length) {
        return true
      }
      if (filters[teamFilterKey]) {
        const inner = Team.getInnerTeamsById(filters[teamFilterKey])
        filters[teamFilterKey] = [filters[teamFilterKey], ...inner]
      }

      return filterKeys.every((key) => {
        const innerValue = Tools.getInnerValue(item, key)
        if (filters[key] === "none") {
          return innerValue === undefined || innerValue === ""
        }else if (Array.isArray(innerValue)) {
          return innerValue.indexOf(filters[key]) !== -1
        }else if (Array.isArray(filters[key])) {
          return filters[key].indexOf(innerValue) !== -1
        } else {
          return innerValue === filters[key]
        }
      })
    }
  }
}

export const structureMixin = {
  computed: {
    structureGetAll() {
      let departments = this.$store.getters["dep/get"]
      const teams = this.$store.getters["team/get"]

      return [...departments, ...teams]
    },
    structureGetTree() {
      const items = [...this.structureGetAll]
      let tmpItems = {}
      items.forEach(item => {
        item.children = []
        tmpItems[item.id] = item
      })

      for (let id in tmpItems) {
        let item = tmpItems[id]
        let parentId = item.parentId

        if (!parentId || !tmpItems[parentId]) {
          continue
        }

        if (!tmpItems[parentId].children) {
          tmpItems[parentId].children = []
        }
        if (!tmpItems[parentId].children.find(child => child.id === item.id)) {
          tmpItems[parentId].children.push(item)
        }
      }

      const tree = Object.values(tmpItems)

      if (this.$can('manage', 'Service')) {
        return tree.filter(item => item.root)
      }

      const currDepId = this.$store.getters['currentUser/get'].depId

      return tree.filter(item => item.id === currDepId)
    }
  },
  methods: {
    structureGetById(id) {
      return this.structureGetAll.find(item => item.id === id)
    }
  }
}