const moment = require('moment')

export class TlDay {
  constructor(date, holidays) {
    const day = moment(date)

    this.day = day
    this.date = day.toText()
    this.month = day.get('month')
    this.number = day.format('DD')

    this.weekend = day.day() === 6 || day.day() === 0
    this.weekDayTitle = day.weekDay()

    this.today = day.toText() === moment().toText()
    this.events = []

    this.month = {
      start: day.startOf('month').toText() === this.date,
      end: day.endOf('month').toText() === this.date
    }

    this.isHoliday = !!holidays.find(d => moment(d).toText() === this.date)
    this.events = 0
  }
}

export class TlMonth {
  constructor(id, date) {
    const day = moment(date)
    this.id = id
    this.days = 0
    this.title = day.locale('ru').format('MMMM')
    this.start = day.startOf('month').toText()
    this.end = day.endOf('month').toText()
    this.fullDaysCount = day.daysInMonth();
    this.year = day.get('year')

    this.hasStart = false
    this.hasEnd = false
  }

  get full() {
    return this.days === this.fullDaysCount
  }

  add(day) {
    if (day.toText() === this.start) this.hasStart = true
    if (day.toText() === this.end) this.hasEnd = true

    this.days++
  }

}

export function defaultFilter (value, search, item) {
  return value != null &&
    search != null &&
    typeof value !== 'boolean' &&
    value.toString().toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) !== -1
}

export const eventColors = [
  'warning',
  'primary',
  'error',
  'success',
  'purple',
  'pink',
  'info',
  'yellow',
  'light-green',
  'teal',
  'grey',
  'lime',
]