export default [
  {
    text: 'Имя',
    value: 'fullName',
    width: '25%',
    align: 'start'
  },
  {
    text: 'Депортамент',
    value: 'depId',
    width: '10%',
    align: 'center',
    visible: ['lg', 'xl']
  },
  {
    text: 'Должность',
    value: 'job.position',
    width: '10%',
    align: 'center',
    visible: ['lg', 'xl']
  },
  {
    text: 'Команда',
    value: 'job.team',
    width: '10%',
    align: 'center',
    visible: ['lg', 'xl']
  },
  {
    text: 'Задачи',
    value: 'job.taskList',
    align: 'center',
    width: '45%',
  },
  {
    text: 'Статус',
    value: 'status',
    align: 'end',
    sortable: true,
    groupable: false,
  },
  {
    text: '',
    value: 'action',
    sortable: false,
    groupable: false,
  },
]