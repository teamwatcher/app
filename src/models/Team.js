import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";

export default class Team {
  __caslSubjectType__="Team"
  static SCHEMA = {
    id: '',
    title: '',
    depId: undefined,
    ownerId: undefined,
    parentId: undefined
  }

  constructor(data) {
    for(let key in Team.SCHEMA) {
      this[key] = data[key] || Team.SCHEMA[key]
    }
  }

  static getAll() {
    return store.getters['team/get']
  }

  static getParentTeamsById(targetId) {
    const teams = Team.getAll();
    const teamsMap = {};

    teams.forEach(team => {
      teamsMap[team.id] = { ...team, children: [] };
    });
    let currentTeam = teamsMap[targetId]
    const parents = []

    if (!currentTeam) return parents

    do {
      parents.push(currentTeam.id)
      currentTeam = teamsMap[currentTeam.parentId]
    }while(currentTeam)

    return parents
  }

  static getInnerTeamsById(id) {
    const childIds = [];
    if (id === undefined) id = ""

    // Рекурсивная функция для поиска дочерних id
    function findChildIds(teams, id) {
      for (let i = 0; i < teams.length; i++) {
        if (teams[i].parentId === id) {
          childIds.push(teams[i].id);
          findChildIds(teams, teams[i].id); // Рекурсивный вызов для поиска дочерних id
        }
      }
    }

    findChildIds(Team.getAll(), id);
    return childIds;
  }

  static getTeamWhereLeader(id) {
    const teams = Team.getAll()
    return teams.find(team => team.ownerId === id)?.id
  }

  static async create(title, parentId, ownerId, depId) {
    try {
      const {data} = await $api.post('/api/team/', {title, ownerId, parentId, depId})

      store.dispatch('team/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Команда создана')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Непредвиденная ошибка")
    }
  }

  static async update({id, title, parentId, ownerId}) {
    try {
      const payload = {title, ownerId}
      if (parentId) payload.parentId = parentId

      const {data} = await $api.post(`/api/team/${id}`, payload)

      store.dispatch('team/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Команда обновлена')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Непредвиденная ошибка")
    }
  }

  static async delete(id, deleteChildren) {
    try {
      const {data} = await $api.delete(`/api/team/${id}`)

      store.dispatch('team/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Команда удалена')
    }catch (e) {
      MessageService.errorMessage(e)
    }
  }
}