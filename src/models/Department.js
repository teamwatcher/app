import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";

export default class Department {
  __caslSubjectType__="Dep"
  static SCHEMA = {
    id: "",
    orgId: "",
    ownerId: "",
    root: false,
    title: "",
    files: [],
    emails: [],
  }

  constructor(data) {
    Object.assign(this, Department.SCHEMA, data)
  }

  static async create(title, parentId, ownerId) {
    try {
      const {data} = await $api.post('/api/dep', {title, ownerId, parentId})

      store.dispatch('dep/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Депортамент создан')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Непредвиденная ошибка")
    }
  }

  static async update({id, title, parentId, ownerId}) {
    try {
      const payload = {title, ownerId}
      if (parentId) payload.parentId = parentId

      const {data} = await $api.post(`/api/dep/${id}`, payload)

      store.dispatch('dep/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Депортамент обновлен')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Непредвиденная ошибка")
    }
  }

  static async delete(id) {
    try {
      const {data} = await $api.delete(`/api/dep/${id}`)

      store.dispatch('dep/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Депортамент удален')
    }catch (e) {
      MessageService.errorMessage(e)
    }
  }
}