export class UserDto {
  __caslSubjectType__="User"
  orgId;
  depId;
  id;
  email;
  fullName;
  displayName;
  status;
  roles;
  job;

  constructor(model) {

    this.orgId = model.orgId
    this.depId = model.depId
    this.id = model.id
    this.email = model.email
    this.fullName = model.fullName
    this.displayName = model.displayName
    this.status = model.status
    this.roles = [...model.roles]

    this.job = {...model.job}
    this.planner = {...model.planner}
  }
}