import store from "@/store";
import $api from "@/services/ApiSetupService";
import {MessageService} from "@/services/MessageService";

export default class Organization {
  __caslSubjectType__="Org"
  id;
  ownerId;
  title;
  rootDep;

  emails;
  files;

  constructor(data) {
    this.id = data.id
    this.ownerId = data.ownerId
    this.title = data.title
    this.rootDep = data.rootDep
    this.emails = data.emails
    this.files = data.files
  }

  getDto() {
    return {
      id: this.id,
      title: this.title
    }
  }

  async save() {
    try {
      const {data} = await $api.post(`/api/org/update`, this.getDto())

      store.dispatch('organization/updateData')
      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  static get() {
    return store.getters['organization/get']
  }
}