import {MessageService} from "@/services/MessageService";
import $api from "@/services/ApiSetupService";
import store from "@/store";

export default class Task {
  __caslSubjectType__="Task"
  static SCHEMA = {
    id: '',
    depId: '',
    title: ''
  }

  constructor(data) {
    for(let key in Task.SCHEMA) {
      this[key] = data[key] || Task.SCHEMA[key]
    }
  }


  static getAll() {
    return store.getters['task/get']
  }

  static async create({title, depId}) {
    try {
      const {data} = await $api.post('/api/task/', {title, depId})

      store.dispatch('task/updateData')
      MessageService.successMessage('Задача создана')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async update({id, title}) {
    try {
      const {data} = await $api.post(`/api/task/${id}`, {title})

      store.dispatch('task/updateData')
      MessageService.successMessage('Задача обновлена')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async delete(id) {
    try {
      const {data} = await $api.delete(`/api/task/${id}`)

      store.dispatch('task/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Задача удалена')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }
}