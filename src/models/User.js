import store from "@/store";
import {MessageService} from "@/services/MessageService";
import $api from "@/services/ApiSetupService";
import {UserDto} from "@/models/UserDto";
import orgService from "@/services/OrgService";
import FileDownload from "js-file-download";
import Team from "@/models/Team";
import {ROLES} from "@/plugins/CONSTANTS";

const STATUSES = {
  CREATED: 0,
  INVITED: 1,
  PRE_ACTIVE: 2,
  ACTIVE: 3,
  DISABLED: 4,
}

export default class User {
  __caslSubjectType__="User"
  static STATUSES = STATUSES

  static ROLES = ROLES

  static SCHEMA = {
    orgId: '',
    depId: '',
    id: undefined,
    email: '',
    fullName: '',
    displayName: '',
    status: STATUSES.CREATED,
    roles: [ROLES.DEP_USER],

    job: {
      tabId: "",
      team: undefined,
      position: undefined,
      taskList: []
    },

    planner: {
      rest: 0,
      additional: 0,
      history: []
    },
    invitedAt: undefined
  }

  constructor(data) {
    Object.assign(this, User.SCHEMA, data)
    if (!this.depId) this.depId = store.getters['currentUser/depId']
    if (!this.orgId) this.orgId = store.getters['organization/getId']
  }

  get isDepOwner() {
    return this.roles.includes(ROLES.DEP_DIRECTOR)
  }

  get isLeader() {
    return this.roles.includes(ROLES.DEP_TEAM_LEADER)
  }

  get isUser() {
    return this.roles.includes(ROLES.DEP_USER)
  }

  get isActive() {
    return this.status === User.STATUSES.ACTIVE
  }

  get isInviting() {
    return this.status === User.STATUSES.INVITED
  }

  get disabled() {
    return this.status === User.STATUSES.DISABLED
  }

  get isTeamLead() {
    return this.role === User.ROLES.LEADER
  }

  get role() {
    const roles = [...this.roles].filter(role => role < 900)
    return roles.sort().at(-1)
  }
  set role(val) {

  }


  getManagers() {
    const teams = []
    if (this.isTeamLead) {
      const leadTeamId = Team.getTeamWhereLeader(this.id)
      teams.push(...Team.getParentTeamsById(leadTeamId))
    }

    teams.push(...Team.getParentTeamsById(this.job.team))

    const uniqTeams = Array.from(new Set(teams))
    const ownerIds = uniqTeams.map(teamId => store.getters['team/getById'](teamId).ownerId)
    ownerIds.push(store.getters['organization/get'].ownerId)

    const managers = ownerIds
      .filter(id => !!id)
      .map(id => store.getters['users/getUserById'](id))

    return managers.filter(user => user.id !== this.id)
  }

  getTeammates() {
    if (!this.job.team) return []

    const users = store.getters['users/getUsersByTeam'](this.job.team)
    return users.filter(user => user.id !== this.id)
  }

  getTaskmates() {
    if (!this.job.taskList || !this.job.taskList.length) return []

    const taskmates = {}

    this.job.taskList.forEach(task => {
      const users = store.getters['users/getUsersByTask'](task).filter(user => user.id !== this.id)
      users.map(user => {
        if (!taskmates[user.id]) taskmates[user.id] = {user, tasks: []}
        taskmates[user.id].tasks.push(task)
      })
    })

    return Object.values(taskmates)
  }

  async create(silent = false) {
    try {
      const user = new UserDto(this)

      const {data} = await $api.post(`/api/auth/preregistration`, {email: user.email, user})

      if (silent) return

      store.dispatch('users/updateData')
      MessageService.successMessage('Пользователь добавлен')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  static async createMany(users) {
    try {
      await Promise.all(users.map(user => user.create(true)))

      store.dispatch('users/updateData')
      MessageService.successMessage("Все пользователи добавлены")
    }
    catch (e) {
      MessageService.errorMessage(e)
    }
  }

  async save(silent = false) {
    try {
      const user = new UserDto(this)

      const {data} = await $api.post(`/api/user/${this.id}`, {user})

      if (silent) return

      store.dispatch('users/updateData')
      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async delete(silent = false) {
    try {
      const {data} = await $api.delete(`/api/user/${this.id}`)

      store.dispatch('users/updateData')
      MessageService.successMessage('Пользователь и все его данные удалены')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async invite(silent = false) {
    try {
      const user = new UserDto(this)
      const {data} = await $api.get(`/api/user/invite/${this.id}`)
      if (silent) return

      store.dispatch('users/updateData')
      MessageService.successMessage('Приглашение отправлено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  static async inviteMany(users) {
    try {
      await Promise.all(users.map(user => user.invite(true)))

      store.dispatch('users/updateData')
      MessageService.successMessage("Приглашения отправлены")
    }
    catch (e) {
      MessageService.errorMessage(e)
    }
  }

  static async import(file) {
    try {
      const formData = new FormData();
      formData.append("import", file);

      const {data} = await $api.post('/api/file/parse/users', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })

      const {validUsers, invalidUsers} = data
      const users = validUsers.map(user => new User(user))

      return {users, errors: invalidUsers}
    }catch (e) {
      return {errors: [{text: e?.response?.data?.message || e.message || "Ошибка импорта"}]}
    }
  }

  static async exportAll() {
    try {
      const {data} = await $api.get(`/api/user/export`, {responseType: "blob"})
      FileDownload(data, 'User List.xlsx');
    }
    catch (e) {
      MessageService.errorMessage(e)
    }
  }

  task = {
    add: async (id) => {
      try {
        const {data} = await $api.post(`/api/user/${this.id}/task`, {id})

        store.dispatch('users/updateData')
        MessageService.successMessage('Задача добавлена')
      }catch (e) {
        MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
      }
    },
    remove: async (id) => {
      try {
        console.log('TRY REMOVE', id)
        const {data} = await $api.delete(`/api/user/${this.id}/task/${id}`)

        store.dispatch('users/updateData')
        MessageService.successMessage('Задача удалена')
      }catch (e) {
        MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
      }
    }
  }
}
