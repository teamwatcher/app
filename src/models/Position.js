import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";

export default class Position {
  __caslSubjectType__="Position"
  static SCHEMA = {
    id: '',
    depId: '',
    title: '',
    displayTitle: '',
  }

  constructor(data) {
    for(let key in Position.SCHEMA) {
      this[key] = data[key] || Position.SCHEMA[key]
    }
  }

  static getAll() {
    return store.getters['position/get']
  }

  static async create({title, displayTitle, depId}) {
    try {
      const {data} = await $api.post('/api/position/', {title, displayTitle, depId})

      store.dispatch('position/updateData')
      MessageService.successMessage('Должность создана')
    }catch (e) {
      MessageService.errorMessage(e)
    }
  }

  static async update({id, title, displayTitle}) {
    try {
      const {data} = await $api.post(`/api/position/${id}`, {title, displayTitle})

      store.dispatch('position/updateData')
      MessageService.successMessage('Должность обновлена')
    }catch (e) {
      MessageService.errorMessage(e)
    }
  }

  static async delete(id) {
    try {
      const {data} = await $api.delete(`/api/position/${id}`)

      store.dispatch('position/updateData')
      store.dispatch('users/updateData')
      MessageService.successMessage('Должность удалена')
    }catch (e) {
      MessageService.errorMessage(e)
    }
  }
}