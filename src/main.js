import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import authService from "@/services/AuthService";
import RichTextEditor from 'rich-text-editor-vuetify'
import '@/assets/style.scss'
import {abilitiesPlugin, Can} from '@casl/vue';
import VCalendar from 'v-calendar';
import VueBreadcrumbs from 'vue-2-breadcrumbs';
import {ability} from "@/plugins/ability";

require('@/plugins/Tools')

const moment = require('moment')
require('moment/locale/ru')
require('@/plugins/initDateTools')

Vue.config.productionTip = false
Vue.prototype.$moment = moment

Vue.use(VueBreadcrumbs);
Vue.use(RichTextEditor)

Vue.use(abilitiesPlugin, ability);

Vue.use(VCalendar, {
  componentPrefix: 'vc',
  masks: { weekdays: 'WW' },
  locale: "ru"
});

Vue.component('Can', Can);

let app = null
authService.onAuthStateChange((user) => {
  store.commit('app/setAccessLevel', user)
  store.dispatch('app/onAuthStateChanged', user)

  if (!app) {
    app = new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})
