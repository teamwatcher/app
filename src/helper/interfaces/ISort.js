import ISortPhase from "@/helper/interfaces/ISortPhase";

export default class ISort {
  value;
  text;
  phase;

  constructor(value, text) {
    this.value = value
    this.text = text
    this.phase = new ISortPhase()
  }
}