export default class IGroup {
  value;
  text;
  handler;

  constructor(value, text, handler) {
    this.value = value
    this.text = text
    this.handler = handler
  }
}