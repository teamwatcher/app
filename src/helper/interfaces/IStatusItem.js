export default class IStatusItem {
  id;
  label;
  color;
  constructor({id, label, color}) {
    this.id = id
    this.label = label
    this.color = color
  }
}