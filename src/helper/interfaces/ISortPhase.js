const STATES = {
  INACTIVE: 0,
  ASC: 1,
  DESC: 2
}

export default class ISortPhase {
  static STATES = STATES

  value;
  constructor() {
    this.value = STATES.INACTIVE
  }

  get active() {
    return this.value !== STATES.INACTIVE
  }

  clear() {
    this.value = STATES.INACTIVE
  }

  next() {
    if (this.value === STATES.DESC) this.clear()
    else this.value++
  }
}