export default class IAction {
  key;
  emit;
  text;
  icon;
  color;
  disabled;
  to;
  menuItem;

  constructor(key, text, icon, color, options = { emit: false, disabled: false, to: false, menuItem: true }) {
    this.key = key
    this.text = text
    this.icon = icon
    this.color = color

    this.emit = options.emit !== undefined ? options.emit : key
    this.disabled = options.disabled !== undefined ? options.disabled : false
    this.to = options.to !== undefined ? options.to : false
    this.menuItem = options.menuItem !== undefined ? options.menuItem : true
  }
}