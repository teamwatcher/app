export default class IFilter {
  items;
  key;
  label;
  itemText;
  itemValue;

  constructor(items, key, label,
              {itemValue = 'id', itemText = 'title', addNone = true} = {}
  ) {
    this.items = [...items]
    if (this.items.length && addNone) {
      const noneItem = {}
      noneItem[itemValue] = 'none'
      noneItem[itemText] = "Нет"
      this.items.push(noneItem)
    }
    this.key = key
    this.label = label
    this.itemText = itemText
    this.itemValue = itemValue
  }
}