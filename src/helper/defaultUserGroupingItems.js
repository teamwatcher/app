import IGroup from "@/helper/interfaces/IGroup";
import store from "@/store";

const groups = [
 new IGroup( 'id','Имени', (id) => store.getters['users/getDisplayNameByID'](id)),
 new IGroup( 'job.position','Должноcтям', (id) => store.getters['position/getById'](id)?.displayTitle),
 new IGroup( 'job.team','Командам', (id) => store.getters['team/getTitleById'](id)),
]

export default [...groups]