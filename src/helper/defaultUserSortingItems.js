import ISort from "@/helper/interfaces/ISort";

const sorts = [
  new ISort('fullName', 'Имени'),
  new ISort('job.team', 'Команде'),
  new ISort('job.position', 'Должности'),
]

export default [...sorts]