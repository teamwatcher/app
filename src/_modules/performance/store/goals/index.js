import Goal from "@/_modules/performance/models/Goal";

export default {
  namespaced: true,
  state: () => ({
    goals: [],
    ready: false
  }),
  getters: {
    get: (s) => s.goals,
    getByPeriodId: (s) => (sid) => s.goals.filter(v => v.periodId === sid),
    getByUserId: (s) => (id) => s.goals.filter(v => v.userId === id),
    getByPeriodAndUserId: (s) => (periodId, userId) => s.goals.filter(v => v.periodId === periodId && v.userId === userId),
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.goals = v.map(item => new Goal(item))
    },
    clear: (s) => s.goals = []
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/performance/goal'
      const setter = 'performance/goals/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}