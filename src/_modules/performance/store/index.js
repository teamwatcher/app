import periods from '@/_modules/performance/store/periods'
import goals from '@/_modules/performance/store/goals'

const modules = {
  periods,
  goals
}

export default {
  namespaced: true,
  modules
}