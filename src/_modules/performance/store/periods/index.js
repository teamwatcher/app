import Period from "@/_modules/performance/models/Period";

export default {
  namespaced: true,
  state: () => ({
    periodList: [],
    ready: false
  }),
  getters: {
    get: (s) => s.periodList,
    getById: (s) => (id) => s.periodList.find(v => v.id === id),
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.periodList = v.map(item => new Period(item))
    },
    clear: (s) => s.periodList = []
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/performance/period'
      const setter = 'performance/periods/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}