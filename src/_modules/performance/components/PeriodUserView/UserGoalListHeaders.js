export default [
  {
    text: 'Название',
    value: 'title',
    sortable: false,
    align: 'left',
    width: '40%'
  },
  {
    text: 'Описание',
    value: 'description',
    sortable: false,
    width: '60%'
  },
  // {
  //   text: 'Оценка',
  //   value: 'mark',
  //   sortable: false,
  //   align: 'center',
  // },
  // {
  //   text: '',
  //   value: 'action',
  //   align: 'end',
  //   cellClass: 'px-0 fit-content border-left',
  //   sortable: false,
  //   groupable: false,
  // },
]