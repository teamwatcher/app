import {createRoute} from "@/router/utils";
import store from "@/store";

const PeriodList = () => import('@/_modules/performance/views/PeriodList')
const PeriodViewer = () => import('@/_modules/performance/views/PeriodViewer')
const PeriodUserView = () => import('@/_modules/performance/views/PeriodUserView')

const r = [
  createRoute('/performance/periods', PeriodList, {title: "Периоды"}),
  createRoute('/performance/periods/:id', PeriodViewer, {
    breadcrumb: function (params) {
      const period = store.getters['performance/periods/getById'](params.id)

      return {
        label: period?.title,
        parent: 'PeriodList'
      }
    }
  }),
  createRoute('/performance/periods/:id/user/:uid', PeriodUserView, {
    breadcrumb: function (params) {
      const userDisplayName = store.getters['users/getDisplayNameByID'](params.uid)

      return {
        label: userDisplayName,
        parent: 'PeriodViewer'
      }
    }
  }),
]


export default [
  {
    "path": "/performance/periods",
    component: PeriodList,
    "meta": {
      "layout": "LayoutApp",
      "title": "Периоды",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      "breadcrumb": "Периоды"
    },
    "name": "PeriodList"
  },
  {
    "path": "/performance/periods/:id",
    component: PeriodViewer,
    "meta": {
      "layout": "LayoutApp",
      "title": "",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      breadcrumb: function (params) {
        const period = store.getters['performance/periods/getById'](params.id)

        return {
          label: period?.title,
          parent: 'PeriodList'
        }
      }
    },
    "name": "PeriodViewer"
  },
  {
    "path": "/performance/periods/:id/user/:uid",
    component: PeriodUserView,
    "meta": {
      "layout": "LayoutApp",
      "title": "",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      breadcrumb: function (params) {
        const userDisplayName = store.getters['users/getDisplayNameByID'](params.uid)

        return {
          label: userDisplayName,
          parent: 'PeriodViewer'
        }
      }
    },
    "name": "PeriodUserView"
  }
]
