export default {
  module: 'vacation',
  header: 'Цели',
  icon: 'mdi-bullseye-arrow',
  to: {name: 'PeriodList'},
  children: [
    {
      title: 'Цели',
      icon: 'mdi-bullseye-arrow',
      to: {name: 'PeriodList'},
    }
  ]
}
