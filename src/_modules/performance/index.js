import routes from '@/_modules/performance/router/routes'
import links from '@/_modules/performance/router/links'

export const performanceRoutes = routes
export const performanceLinks = links

export const UserPeriodList = () => import("@/_modules/performance/components/UserPeriodList/UserPeriodList")

