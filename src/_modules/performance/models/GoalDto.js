export class GoalDto {
  id;
  userId;
  periodId;

  title;
  description;
  weight;
  progress;
  position;

  status;

  constructor(model) {
    this.id = model._id
    this.userId = model.userId;
    this.periodId = model.periodId;

    this.title = model.title;
    this.description = model.description
    this.weight = model.weight;
    this.progress = model.progress;
    this.position = model.position;
    this.status = model.status;
  }
}