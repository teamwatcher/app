import Goal from "@/_modules/performance/models/Goal";
import {GoalDto} from "@/_modules/performance/models/GoalDto";
import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";

export default class GoalList extends Array {
  addItem() {
    this.push(new Goal())
  }

  removeItem(id) {
    const index = this.findIndex(item => item.id === id)

    if (index > -1) {
      this.splice(index, 1);
    }
  }

  async save(userId, periodId) {
    try {
      await Promise.all(this.map((item, idx) => {
        item.position = idx + 1
        item.userId = userId
        item.periodId = periodId

        const goal = new GoalDto(item)
        return $api.post(`/api/performance/goal`, goal)
      }))

      store.dispatch('performance/goals/updateData')
      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage("Ошибка сохранения")
    }
  }

  async update() {
    try {
      await Promise.all(this.map((item, idx) => {
        item.position = idx + 1

        const goal = new GoalDto(item)
        return $api.post(`/api/performance/goal/${item.id}`, {data: goal})
      }))

      store.dispatch('performance/goals/updateData')
      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage("Ошибка сохранения")
    }
  }
}