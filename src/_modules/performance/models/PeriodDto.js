export class PeriodDto {
  id;
  title;
  start;
  duration;
  status;

  constructor(model) {
    this.id = model._id
    this.title = model.title;
    this.start = model.start;
    this.duration = model.duration;
    this.status = model.status
  }
}