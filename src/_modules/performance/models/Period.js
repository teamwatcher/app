import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";
import {PeriodDto} from "@/_modules/performance/models/PeriodDto";
import IStatusItem from "@/helper/interfaces/IStatusItem";

const STATUSES = {
  ACTIVE: 0,
  CLOSED: 1
}

const STATUSES_DATA = {}
STATUSES_DATA[STATUSES.ACTIVE] = new IStatusItem({id: STATUSES.ACTIVE, label: 'Активен', color: 'success'})
STATUSES_DATA[STATUSES.CLOSED] = new IStatusItem({id: STATUSES.CLOSED, label: 'Закрыт', color: 'error'})

export default class Period {
  static STATUSES = STATUSES
  static STATUSES_DATA = STATUSES_DATA

  static SCHEMA = {
    id: '',
    title: '',
    start: '',
    end: '',
    duration: 12,
    status: STATUSES.ACTIVE,
    progress: 0,
    isStarted: false,
    isFinished: false,
    isActive: false
  }

  constructor(data) {
    Object.assign(this, Period.SCHEMA, data)
    this.start = new Date(this.start)
    this.end = new Date(this.end)
  }

  // get isActive() {
  //   return this.status === STATUSES.ACTIVE
  // }

  async create() {
    try {
      const period = new PeriodDto(this)
      const {data} = await $api.post(`/api/performance/period`, period)

      store.dispatch('performance/periods/updateData')
      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async update() {
    try {
      const period = new PeriodDto(this)
      const {data} = await $api.post(`/api/performance/period/${this.id}`, {data: period})

      store.dispatch('performance/periods/updateData')
      MessageService.successMessage('Обновлено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }


  async delete() {
    try {
      const {data} = await $api.delete(`/api/performance/period/${this.id}`)

      store.dispatch('performance/periods/updateData')
      MessageService.successMessage('Удалён')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }


  async activate() {
    try {
      const {data} = await $api.get(`/api/performance/period/${this.id}/activate`)

      store.dispatch('performance/periods/updateData')
      MessageService.successMessage('Активирован')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }


  async close() {
    try {
      const {data} = await $api.get(`/api/performance/period/${this.id}/close`)

      store.dispatch('performance/periods/updateData')
      MessageService.successMessage('Закрыт')
    } catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }
}