import IStatusItem from "@/helper/interfaces/IStatusItem";
import {GoalDto} from "@/_modules/performance/models/GoalDto";
import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";

const STATUSES = {
  ACTIVE: 0, CLOSED: 1
}

const STATUSES_DATA = {}
STATUSES_DATA[STATUSES.ACTIVE] = new IStatusItem({id: STATUSES.ACTIVE, label: 'Активен', color: 'success'})
STATUSES_DATA[STATUSES.CLOSED] = new IStatusItem({id: STATUSES.CLOSED, label: 'Закрыт', color: 'error'})

export default class Goal {
  static STATUSES = STATUSES
  static STATUSES_DATA = STATUSES_DATA

  static SCHEMA = {
    id: '', userId: '', periodId: '',

    title: '', description: '', weight: null, progress: 0, position: 0,

    status: STATUSES.ACTIVE
  }

  constructor(data) {
    Object.assign(this, Goal.SCHEMA, data)
    if (!this.id) {
      this.id = 'tmp-' + Date.now()
    }
  }

  get completed() {
    return this.progress === 100
  }


  static async SAVE(oldItems, newItems, userId, periodId) {
    const oldItemsMap = {}
    oldItems.forEach(item => oldItemsMap[item.id] = item)

    const newItemsMap = {}
    newItems.forEach(item => newItemsMap[item.id] = item)

    const allIds = Array.from(new Set([...Object.keys(oldItemsMap), ...Object.keys(newItemsMap)]))

    const deletingIds = []
    const activeIds = []

    allIds.map(id => {
      const isOld = !!oldItemsMap[id]
      const isNew = !!newItemsMap[id]

      if (isOld && !isNew) {
        deletingIds.push(id)
      } else {
        activeIds.push(id)
      }
    })

    try {
      await Promise.all([Promise.all(deletingIds.map(id => {
        oldItemsMap[id].delete()
      })), Promise.all(activeIds.map((id, idx) => {
        const goal = newItemsMap[id]
        goal.position = idx + 1
        goal.userId = userId
        goal.periodId = periodId

        console.log(goal)

        if (!goal.id.includes('tmp-')) {
          return goal.update()
        } else {
          return goal.create()
        }
      }))])

      store.dispatch('performance/goals/updateData')
      MessageService.successMessage('Данные сохранены')
    } catch (e) {
      MessageService.errorMessage("Ошибка сохранения")
    }
  }

  async create() {
    const goal = new GoalDto(this)
    return $api.post(`/api/performance/goal`, goal)
  }

  async update() {
    const goal = new GoalDto(this)
    return await $api.post(`/api/performance/goal/${this.id}`, {data: goal})
  }

  async delete() {
    return await $api.delete(`/api/performance/goal/${this.id}`)
  }


  async updateProgress() {
    try {
      const progress = this.progress
      await $api.post(`/api/performance/goal/${this.id}/progress`, {progress})
      MessageService.baseMessage("Обновлено")
    } catch (e) {
      MessageService.errorMessage("Ошибка")
    }
  }
}