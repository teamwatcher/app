export default [
  {
    text: 'Название',
    value: 'title',
    width: '25%'
  },
  {
    text: 'Год',
    value: 'year',
    width: '25%'
  },
  {
    text: 'Статус',
    value: 'status',
    sortable: true,
    width: '10%'
  },
  {
    text: '',
    value: 'locked',
    sortable: false,
    width: '40%'
  },
  {
    text: '',
    value: 'action',
    align: 'end',
    cellClass: 'px-0 fit-content border-left',
    sortable: false,
    groupable: false,
  },
]