export default [
  {
    text: 'Имя',
    value: 'displayName',
    cellClass: 'px-0',
    align: 'start',
    width: '80%',
  },
  {
    text: 'Доп. дни',
    value: 'planner.additional',
    align: 'center',
    cellClass: 'px-0 fit-content border-left',
    width: '10%'
  },
  {
    text: 'Остаток',
    value: 'planner.rest',
    align: 'center',
    cellClass: 'px-0 fit-content border-left',
    width: '10%'
  },
  // {
  //   text: '',
  //   value: 'action',
  //   align: 'end',
  //   cellClass: 'px-0 fit-content border-left',
  //   sortable: false,
  //   groupable: false,
  // },
]