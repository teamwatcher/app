export default [
  {
    text: 'Имя',
    value: 'user.id',
    cellClass: 'pa-0 px-2',
    class: 'text-no-wrap',
    align: 'start',
    width: '20%',
    groupable: false
  },
  {
    text: 'Даты',
    value: 'start',
    class: 'text-no-wrap',
    width: '30%',
    align: 'center',
  },
  {
    text: 'Дней',
    value: 'days',
    class: 'text-no-wrap',
    width: '5%',
    align: 'center',
  },
  {
    text: 'Статус',
    value: 'status',
    align: 'center',
    width: '10%',
    class: 'pa-0 text-no-wrap',
    cellClass: 'fit-content',
  },
  {
    text: 'Комментарий',
    value: 'comment',
    align: 'center',
    width: '35%',
    class: 'pa-0 text-no-wrap',
    cellClass: 'fit-content',
    sortable: false,
  },
  {
    text: '',
    value: 'action',
    align: 'end',
    cellClass: 'px-0 fit-content',
    sortable: false,
    groupable: false
  },
]