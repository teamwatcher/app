export default [
  {text: 'Даты', value: 'start', align: 'start'},
  {text: 'Дней', value: 'days', align: 'start'},
  {text: 'Статус', value: 'status', align: 'center'},
  {text: 'Комментарий', value: 'comment', align: 'center', class: 'max-w-35'},
  {
    text: '',
    value: 'action',
    align: 'end',
    sortable: false,
    cellClass: 'px-0',
    class: ''
  },
]