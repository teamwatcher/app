import store from "@/store";

export const colors = {
  holidays: "#2196f3",
  workdays: "#fb8c00",
  weekends: "#da3e3e"
}

export const Weekends = {
  key: 'weekends',
  highlight: {
    style: {
      display: 'none'
    },
    fillMode: 'outline',
    contentStyle: {
      color: colors.weekends
    }
  },
  dates: {
    weekdays: [1, 7]
  }
}

export const HoverableAll = {
  key: 'HoverableAll',
  popover: {
    visibility: 'hover',
  },
  dates: {weekdays: [1, 2, 3, 4, 5, 6, 7]}
}



export const Exception = (type, dates) => {
  return {
    key: type,
    highlight: {
      style: {
        display: 'none'
      },
      fillMode: 'outline',
        contentStyle: {
        color: colors[type]
      }
    },
    dates: dates
  }
}

export const OtherExistsVacation = (vacation) => {
  return {
    key: `OtherExistsVacation-${vacation.id}`,
    highlight: {
      color: 'green',
      fillMode: 'light',
    },
    customData: {
      name: 'Существующий отпуск',
      tasks: '',
      dates: {
        start: vacation.start,
        end: vacation.end
      }
    },
    popover: {
      visibility: 'hover',
      label: "name",
      placement: 'top',
    },
    dates: {
      start: vacation.start,
      end: vacation.end
    }
  }
}




export const getUsersVacationsBar = (users, scheduleId, keyPrefix, color) => {
  const bar = {
    key: keyPrefix + 'bar',
    bar: {
      color,
    },
    dates: [],
  }

  users.forEach(user => {
    store.getters['planner/vacations/getByScheduleAndUserId'](scheduleId, user.id)
      .filter(vacation => vacation.active)
      .forEach(({start, end}) => {
        bar.dates.push({start, end})
      })
  })

  return bar
}

export const getUsersVacationsPopovers = (users, scheduleId, keyPrefix, color, tasks = []) => {
  return users.map((user, idx) => {
    const vacations = store.getters['planner/vacations/getByScheduleAndUserId'](scheduleId, user.id)
      .filter(vacation => vacation.active)

    const createPopover = ({start, end, id}) => ({
      key: `${keyPrefix}-${user.id}-${id}-popover`,
      highlight: {
        color,
        style: {
          display: 'none'
        },
      },
      popover: {
        visibility: 'hover',
        placement: 'top'
      },
      customData: {
        name: user.displayName,
        dates: {start, end},
        tasks: tasks[idx]
      },
      dates: {start, end}
    })

    return vacations.map(createPopover)
  })
}
