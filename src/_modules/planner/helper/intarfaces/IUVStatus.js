export default class IUVStatus {
  days;
  limit;
  title;
  description;

  constructor(model) {
    this.days = model.days;
    this.limit = model.limit;
    this.title = model.title;
    this.description = model.description;
  }
}