import routes from '@/_modules/planner/router/routes'
import links from '@/_modules/planner/router/links'
import settingRoutes from "@/_modules/planner/router/settingRoutes";
import settingLinks from "@/_modules/planner/router/settingLinks";

export const plannerRoutes = routes
export const plannerLinks = links
export const plannerSettingsRoutes = settingRoutes
export const plannerSettingsLinks = settingLinks

export const UserScheduleList = () => import("@/_modules/planner/components/UserSchedulesList")
