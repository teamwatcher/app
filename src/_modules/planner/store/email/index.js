import {Planner} from "@/_modules/planner/models/Planner";
import PlannerEmail from "@/_modules/planner/models/PlannerEmail";

export default {
  namespaced: true,
  state: () => ({
    email: null
  }),
  getters: {
    get: (s) => s.email,
  },
  mutations: {
    set: (s, v) => {
      s.email = new PlannerEmail(v)
    },
    clear: (s) => {
      s.email = null
    }
  },
  actions: {
    initialize({rootGetters, commit}) {
      const {emails} = rootGetters['organization/get']
      const email = emails?.find(email => email.name === Planner.EmailComing)

      if (email) commit('set', email)
    },
  }
}
