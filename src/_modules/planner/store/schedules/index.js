import Schedule from "@/_modules/planner/models/Schedule";

export default {
  namespaced: true,
  state: () => ({
    schedules: [],
    ready: false
  }),
  getters: {
    get: (s) => s.schedules,
    getById: (s) => (id) => s.schedules.find(schedule => schedule.id === id),
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.schedules = v.map(item => new Schedule(item))
    },
    clear: (s) => s.schedules = []
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/planner/schedule'
      const setter = 'planner/schedules/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  }
}