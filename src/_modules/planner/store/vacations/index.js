import Vacation from "@/_modules/planner/models/Vacation";

export default {
  namespaced: true,
  state: () => ({
    vacations: [],
    ready: false
  }),
  getters: {
    get: (s) => s.vacations,
    getByScheduleId: (s) => (sid) => s.vacations.filter(v => v.scheduleId === sid),
    getByScheduleAndUserId: (s) => (sid, id) => s.vacations.filter(v => v.scheduleId === sid && v.userId === id),
    isReady: (s) => s.ready
  },
  mutations: {
    set: (s, v) => {
      if (!s.ready) s.ready = true

      s.vacations = v.map(item => new Vacation(item))
    },
    clear: (s) => s.vacations = []
  },
  actions: {
    initialize({dispatch}) {
      return dispatch('updateData')
    },
    onLogOut({commit}) {
      commit('clear')
    },
    updateData({dispatch}) {
      const path = '/api/planner/vacation'
      const setter = 'planner/vacations/set'

      return dispatch('DB/updateData', {path, setter}, {root: true})
    },
  },
}