import {Planner} from "@/_modules/planner/models/Planner";

export default {
  namespaced: true,
  state: () => ({
    file: null
  }),
  getters: {
    get: (s) => s.file || {},
    isExist: (s) => s.file !== null
  },
  mutations: {
    set: (s, v) => {
      s.file = v
    },
    clear: (s) => {
      s.file = null
    }
  },
  actions: {
    initialize({rootGetters, commit}) {
      const {files} = rootGetters['organization/get']

      const blankFile = files?.find(file => file.name === Planner.BlankFileName)

      if (blankFile) commit('set', blankFile)
    },
  }
}
