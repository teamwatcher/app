import vacations from '@/_modules/planner/store/vacations'
import schedules from '@/_modules/planner/store/schedules'
import blank from "@/_modules/planner/store/blank";
import email from "@/_modules/planner/store/email";

const modules = {
  vacations,
  schedules,
  blank,
  email
}

export default {
  namespaced: true,
  modules
}