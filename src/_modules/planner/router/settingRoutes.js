const SettingPlannerCommon = () => import("@/_modules/planner/views/SettingPlannerCommon")
const SettingPlannerPermission = () => import("@/_modules/planner/views/SettingPlannerPermission")

export default [
  {
    path: 'planner/common',
    name: 'SettingPlannerCommon',
    component: SettingPlannerCommon,
    meta: {
      breadcrumb: "hidden",
      layout: 'LayoutApp',
      title: 'Настройки',
      protected: {
        accessLevel: [2]
      },
    }
  },
  {
    path: 'planner/permission',
    name: 'SettingPlannerPermission',
    component: SettingPlannerPermission,
    meta: {
      breadcrumb: "hidden",
      layout: 'LayoutApp',
      title: 'Настройки',
      protected: {
        accessLevel: [2]
      },
    }
  },
]