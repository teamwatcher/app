import store from "@/store";

const UserSchedulesView = () => import('@/_modules/planner/views/UserSchedulesView')
const UserVacationsView = () => import('@/_modules/planner/views/UserVacationsView')

const PlannerDashboard = () => import('@/_modules/planner/views/PlannerDashboard')

const ScheduleHome = () => import('@/_modules/planner/views/ScheduleList')
const ScheduleStatistic = () => import('@/_modules/planner/views/ScheduleStatistic')
const ScheduleViewer = () => import('@/_modules/planner/views/ScheduleViewer')
const ScheduleViewerList = () => import('@/_modules/planner/components/ScheduleViewer/ScheduleViewerList'  )
const ScheduleViewerTimeline = () => import('@/_modules/planner/components/ScheduleViewer/ScheduleViewerTimeline'  )

export default [
  {
    "path": "/planner/user",
    component: UserSchedulesView,
    "meta": {
      "layout": "LayoutApp",
      "title": "",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      "breadcrumb": {
        "label": "Мои отпуска"
      }
    },
    "name": "UserSchedulesView"
  },
  {
    "path": "/planner/dashboard",
    component: PlannerDashboard,
    "meta": {
      "layout": "LayoutApp",
      "title": "",
      "protected": {
        "accessLevel": [
          2
        ]
      }
    },
    "name": "PlannerDashboard"
  },
  {
    "path": "/planner/user/:uid/schedules/:id",
    component: UserVacationsView,
    "meta": {
      "layout": "LayoutApp",
      "title": "",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      breadcrumb: function (params) {
        const currentUid = store.getters['currentUser/id']
        const schedule = store.getters['planner/schedules/getById'](params.id)
        let parent = "Employee"

        if (currentUid === params.uid) {
          parent = "UserSchedulesView"
        }

        return {
          label: schedule?.title,
          parent
        }
      },
    },
    "name": "UserVacationsView"
  },
  {
    "path": "/planner/schedules",
    component: ScheduleHome,
    "meta": {
      "layout": "LayoutApp",
      "title": "Графики отпусков",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      "breadcrumb": "Графики отпусков"
    },
    "name": "ScheduleHome"
  },
  {
    "path": "/planner/schedules/:id",
    component: ScheduleViewer,
    "meta": {
      "layout": "LayoutApp",
      "title": "Просмотр",
      "protected": {
        "accessLevel": [
          2
        ]
      },
      breadcrumb: function (params) {
        const schedule = store.getters['planner/schedules/getById'](params.id)

        return {
          label: schedule?.title,
          parent: "ScheduleHome"
        }
      },
    },
    "name": "ScheduleViewer",
    "redirect": {
      "name": "ScheduleViewerList"
    },
    "children": [
      {
        "path": "list",
        component: ScheduleViewerList,
        "meta": {
          "layout": "LayoutApp",
          "title": "Список",
          "protected": {
            "accessLevel": [
              2
            ]
          },
          "breadcrumb": "Список"
        },
        "name": "ScheduleViewerList"
      },
      {
        "path": "timeline",
        component: ScheduleViewerTimeline,
        "meta": {
          "layout": "LayoutApp",
          "title": "Таймлайн",
          "protected": {
            "accessLevel": [
              2
            ]
          },
          "breadcrumb": "Таймлайн"
        },
        "name": "ScheduleViewerTimeline"
      }
    ]
  },
]


// const r = [
//   createRoute('/planner/user', UserSchedulesView, {
//     breadcrumb: {
//       label: "Мои отпуска",
//     }
//   }),
//   createRoute('/planner/manage', PlannerDashboard),
//   createRoute('/planner/user/:uid/schedules/:id', UserVacationsView, {
//     breadcrumb: function (params) {
//       const currentUid = store.getters['currentUser/id']
//       const schedule = store.getters['planner/schedules/getById'](params.id)
//       let parent = "Employee"
//
//       if (currentUid === params.uid) {
//         parent = "UserSchedulesView"
//       }
//
//       return {
//         label: schedule?.title,
//         parent
//       }
//     },
//   }),
//   createRoute('/planner/schedules', ScheduleHome, {title: 'Графики отпусков',}),
//   createRoute('/planner/schedules/:id', ScheduleViewer, {
//     title: 'Просмотр',
//     redirect: {name: 'ScheduleViewerList'},
//     breadcrumb: function (params) {
//       const schedule = store.getters['planner/schedules/getById'](params.id)
//
//       return {
//         label: schedule?.title,
//         parent: "ScheduleHome"
//       }
//     },
//     children: [
//       ['list', ScheduleViewerList, {title: "Список"}],
//       ['timeline', ScheduleViewerTimeline, {title: "Таймлайн"}]
//     ],
//   }),
//   createRoute('/planer/schedules/stat/:id', ScheduleStatistic, {title: 'Статистика',})
// ]
