import store from "@/store";

const uid = store.getters['currentUser/id']

export default {
  module: 'vacation',
  header: 'Отспуска',
  icon: 'mdi-calendar-outline',
  children: [
    {to: {name: 'ScheduleHome', params: {uid}}, icon: 'mdi-calendar-multiple', title: 'Графики'},
    {to: {name: 'UserSchedulesView'}, icon: 'mdi-calendar-account-outline', title: 'Мои отпуска'},
    {to: {name: 'PlannerDashboard'}, icon: 'mdi-file-cog-outline', title: 'Управление'},
  ]
}