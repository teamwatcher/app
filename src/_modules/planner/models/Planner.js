import $api from "@/services/ApiSetupService";
import {MessageService} from "@/services/MessageService";
import store from "@/store";
import FileService from "@/services/FileService";

export class Planner {
  __caslSubjectType__="Planner"
  static BlankFileName = 'PlannerBlank'
  static EmailComing = 'PlannerEmailComing'

  static async uploadBlank(file) {
    try {
      const formData = new FormData();
      formData.append(Planner.BlankFileName, file);

      const {data} = await $api.post('/api/planner/setting/blank', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })

      await store.dispatch('organization/updateData')
      store.dispatch('planner/blank/initialize')
      MessageService.successMessage('Файл загружен')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async downloadBlank() {
    const path = '/api/planner/setting/blank'
    await FileService.downloadFile(path, 'Шаблон заявления.docx')
  }

  static async downloadExample() {
    const path = '/api/planner/setting/blank/example'
    await FileService.downloadFile(path, 'Пример заявления.docx')
  }
}