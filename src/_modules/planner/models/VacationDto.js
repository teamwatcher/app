export class VacationDto {
  __caslSubjectType__="Vacation"
  id;
  userId;
  scheduleId;
  start;
  end;
  days;
  comment;

  constructor(model) {
    this.id = model._id
    this.userId = model.userId
    this.scheduleId = model.scheduleId
    this.start = model.start
    this.end = model.end
    this.days = model.days
    this.comment = model.comment
  }
}