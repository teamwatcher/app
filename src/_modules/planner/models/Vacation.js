import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";
import {VacationDto} from "@/_modules/planner/models/VacationDto";
import IStatusItem from "@/helper/interfaces/IStatusItem";
import FileDownload from "js-file-download";
const STATUSES = {
  DRAFT: 0,
  SENT: 1,
  APPROVED: 2,
  REJECTED: 3
}

const STATUSES_DATA = {}

STATUSES_DATA[STATUSES.DRAFT] = new IStatusItem({id: STATUSES.DRAFT, label: 'Черновик', color: 'secondary'})
STATUSES_DATA[STATUSES.SENT] = new IStatusItem({id: STATUSES.SENT, label: 'На утверждении', color: 'warning'})
STATUSES_DATA[STATUSES.APPROVED] = new IStatusItem({id: STATUSES.APPROVED, label: 'Утверждён', color: 'success'})
STATUSES_DATA[STATUSES.REJECTED] = new IStatusItem({id: STATUSES.REJECTED, label: 'Отклонён', color: 'error'})

export default class Vacation {
  __caslSubjectType__="Vacation"
  static STATUSES = STATUSES
  static STATUSES_DATA = STATUSES_DATA

  static EVENT_STATUSES = {
    CREATED: 0,
    UPDATED: 1,
    SENT_APPROVING: 2,
    CANCEL_APPROVING: 3,
    REJECTED: 4,
    APPROVED: 5,
  }

  static SCHEMA = {
    id: '',
    userId: '',
    scheduleId: '',

    start: null,
    end: null,
    status: Vacation.STATUSES.DRAFT,
    days: 0,

    comment: '',
    history: [],
  }

  constructor(data) {
    Object.assign(this, Vacation.SCHEMA, data)
  }

  get draft() {
    return this.status === Vacation.STATUSES.DRAFT
  }

  get sent() {
    return this.status === Vacation.STATUSES.SENT// || this.status === Vacation.STATUSES.APPROVED
  }

  get active() {
    return this.status === Vacation.STATUSES.SENT || this.status === Vacation.STATUSES.APPROVED
  }

  get approved() {
    return this.status === Vacation.STATUSES.APPROVED
  }

  get rejected() {
    return this.status === Vacation.STATUSES.REJECTED
  }

  static async getDaysCount(start, end, scheduleId) {
    try {
      const {data} = await $api.post(`/api/planner/vacation/calc/days`, {start, end, scheduleId})
      return data
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка получения данных")
    }
  }

  async create() {
    try {
      const vacation = new VacationDto(this)
      const {data} = await $api.post(`/api/planner/vacation`, vacation)

      await store.dispatch('planner/vacations/updateData')
      MessageService.successMessage('Отпуск добавлен')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async update() {
    try {
      const vacation = new VacationDto(this)
      const {data} = await $api.post(`/api/planner/vacation/${this.id}`, {data: vacation})

      await store.dispatch('planner/vacations/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('Изменения сохранены')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async delete() {
    try {
      const {data} = await $api.delete(`/api/planner/vacation/${this.id}`)

      await store.dispatch('planner/vacations/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('Отпуск удалён')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async toApproving() {
    try {
      const {data} = await $api.get(`/api/planner/vacation/${this.id}/approving`)

      await store.dispatch('planner/vacations/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('Отправлено на утверждение')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async toDraft() {
    try {
      const {data} = await $api.get(`/api/planner/vacation/${this.id}/draft`)

      await store.dispatch('planner/vacations/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('Отпуск отозван')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async approve() {
    try {
      const {data} = await $api.get(`/api/planner/vacation/${this.id}/approve`)

      await store.dispatch('planner/vacations/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('Отпуск утверждён')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async reject() {
    try {
      const {data} = await $api.get(`/api/planner/vacation/${this.id}/reject`)

      await store.dispatch('planner/vacations/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('Отпуск отклонён')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async download() {
    try {
      const {data} = await $api.get(`/api/planner/vacation/${this.id}/blank`, {responseType: "blob"})

      const fullName = store.getters['users/getUserById'](this.userId)?.fullName
      const fileName = `Заявление на ОТПУСК очередной оплачиваемый ${fullName}`
      FileDownload(data, `${fileName}.docx`);
    }
    catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }


  async emailTest() {
    try {
      const {data} = await $api.get(`/api/planner/vacation/${this.id}/email`)
      console.log(data)
      MessageService.successMessage('Отправлено')
    }
    catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }
}