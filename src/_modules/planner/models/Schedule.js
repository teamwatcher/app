import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";
import {ScheduleDto} from "@/_modules/planner/models/ScheduleDto";
import IStatusItem from "@/helper/interfaces/IStatusItem";
const STATUSES = {
  DRAFT: 100,
  ACTIVE: 200,
  ACTIVE_LOCKED: 250,
  CLOSED: 900
}

const STATUSES_DATA = {}
STATUSES_DATA[STATUSES.DRAFT] = new IStatusItem({id: STATUSES.DRAFT, label: 'Черновик', color: 'secondary'})
STATUSES_DATA[STATUSES.ACTIVE] = new IStatusItem({id: STATUSES.ACTIVE, label: 'На заполнении', color: 'success'})
STATUSES_DATA[STATUSES.ACTIVE_LOCKED] = new IStatusItem({id: STATUSES.ACTIVE, label: 'На заполнении', color: 'success'})
STATUSES_DATA[STATUSES.CLOSED] = new IStatusItem({id: STATUSES.CLOSED, label: 'Закрыт', color: 'error'})


export default class Schedule {
  __caslSubjectType__="Schedule"
  static STATUSES = STATUSES
  static STATUSES_DATA = STATUSES_DATA

  static SCHEMA = {
    id: '',
    title: '',
    year: '',

    status: Schedule.STATUSES.DRAFT,
    lock: false,
    holidayList: [],

    addVacationDays: 28,
    addIndividualDays: true,
  }

  constructor(data) {
    Object.assign(this, Schedule.SCHEMA, data)
    this.holidayList = this.holidayList.map(d => new Date(d))
  }

  get startDate() {
    return new Date(`${this.year}-01-01`)
  }
  get endDate() {
    return new Date(`${this.year}-12-31`)
  }

  get isDraft() {
    return this.status === Schedule.STATUSES.DRAFT
  }

  get isActive() {
    return this.status === Schedule.STATUSES.ACTIVE || this.status === Schedule.STATUSES.ACTIVE_LOCKED
  }

  get isClosed() {
    return this.status === Schedule.STATUSES.CLOSED
  }

  get isLocked() {
    return this.status === Schedule.STATUSES.ACTIVE_LOCKED
  }

  async create() {
    try {
      const schedule = new ScheduleDto(this)
      const {data} = await $api.post(`/api/planner/schedule`, schedule)

      await store.dispatch('planner/schedules/updateData')
      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка сохранения")
    }
  }

  async update() {
    try {
      const schedule = new ScheduleDto(this)
      const {data} = await $api.post(`/api/planner/schedule/${this.id}`, {data: schedule})

      await store.dispatch('planner/schedules/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('График обновлён')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  async toggleLock() {
    try {
      if (!this.isActive) return

      const data = {status: STATUSES.ACTIVE_LOCKED}

      if (this.status === STATUSES.ACTIVE_LOCKED) data.status = STATUSES.ACTIVE

      await $api.post(`/api/planner/schedule/${this.id}`, {data})

      await store.dispatch('planner/schedules/updateData')
      await store.dispatch('users/updateData')
      if (data.status === STATUSES.ACTIVE_LOCKED) MessageService.successMessage('График заблокирован')
      else MessageService.successMessage('График разблокирован')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }


  async delete() {
    try {
      const {data} = await $api.delete(`/api/planner/schedule/${this.id}`)

      await store.dispatch('planner/schedules/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('График удалён')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  async draft() {
    try {
      const {data} = await $api.get(`/api/planner/schedule/${this.id}/draft`)

      await store.dispatch('planner/schedules/updateData')
      MessageService.successMessage('График перемещён в черновики')
    } catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  async activate() {
    try {
      const {data} = await $api.get(`/api/planner/schedule/${this.id}/activate`)

      await store.dispatch('planner/schedules/updateData')
      await store.dispatch('users/updateData')
      MessageService.successMessage('График активирован')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  async close() {
    try {
      const {data} = await $api.get(`/api/planner/schedule/${this.id}/close`)

      await store.dispatch('planner/schedules/updateData')
      MessageService.successMessage('График закрыт')
    } catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }
}