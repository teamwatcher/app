import {ScheduleDto} from "@/_modules/planner/models/ScheduleDto";
import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";

export default class PlannerEmail {
  __caslSubjectType__="Email"
  id;
  when;
  name;
  subject;
  addManagers;
  addCC;
  body;

  constructor(model) {
    this.id = model._id
    this.when = model.when
    this.name = model.name;
    this.subject = model.subject
    this.addManagers = model.addManagers
    this.addCC = model.addCC || null
    this.body = model.body
  }


  async save() {
    try {
      const {data} = await $api.post(`/api/planner/setting/email/coming`, {id: this.id, data: this})

      MessageService.successMessage('Сохранено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }
}