import $api from "@/services/ApiSetupService";
import store from "@/store";
import {MessageService} from "@/services/MessageService";
import FileDownload from "js-file-download";

export default class Manage {
  static async setAdditionalDays(userId, days) {
    try {
      const {data} = await $api.post(`/api/planner/manage/set/additional`, {id: userId, days})

      await store.dispatch('users/updateData')
      MessageService.successMessage('Обновлено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async setRestDays(userId, days, comment) {
    try {
      const {data} = await $api.post(`/api/planner/manage/set/rest`, {id: userId, days, comment})

      await store.dispatch('users/updateData')
      MessageService.successMessage('Обновлено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async addManyRestDays(users, days, comment) {
    try {
      const {data} = await $api.post(`/api/planner/manage/add/rest/users`, {users, days, comment})

      await store.dispatch('users/updateData')
      MessageService.successMessage('Обновлено')
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async scheduleReport(id) {
    try {
      const {data} = await $api.post(`/api/planner/report/schedule`, {id}, {responseType: "blob"})

      const fileName = store.getters['planner/schedules/getById'](id).title
      FileDownload(data, `${fileName}.xlsx`);
    }
    catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }

  static async userReport(id, details = false) {
    try {
      const {data} = await $api.post(`/api/planner/report/user`, {id, details}, {responseType: "blob"})

      const fileName = store.getters['users/getUserById'](id).fullName
      FileDownload(data, `${fileName}.xlsx`);
    }
    catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }


  static async orgReport() {
    try {
      const {data} = await $api.get(`/api/planner/report/org`,{responseType: "blob"})

      const fileName = store.getters['organization/get'].title
      FileDownload(data, `${fileName}.xlsx`);
    }
    catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка")
    }
  }
}