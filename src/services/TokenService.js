class TokenService {
  tokenKey = '_t';
  token = null;

  set token(token) {
    // localStorage.setItem(this.tokenKey, token)
    this.token = token
  }

  get token() {
    // return localStorage.getItem(this.tokenKey)
    return this.token
  }

  clearToken() {
    // localStorage.removeItem(this.tokenKey)
    this.token = null
  }
}

export default new TokenService()