import router from "@/router";
import {MessageService} from "@/services/MessageService";
import store from "@/store";
import orgService from "@/services/OrgService";
import $api from "@/services/ApiSetupService";
import tokenService from "@/services/TokenService";
import axios from "axios";
import User from "@/models/User";

class AuthService {
  constructor() {
    this.checkAuthState()
  }

  async registrationByEmail({email, password, fullName, org}) {
    try {
      const {data} = await $api.post('/api/auth/registration', { email, password, fullName, org})

      store.commit('auth/setEmail', email)
      await router.push({name: 'EmailSending'})
      MessageService.successMessage("Письмо отправлено")
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка регистрации")
    }
  }

  async login({email, password}) {
    try {
      const {data} = await $api.post('/api/auth/login', { email, password })
      const emailConfirm = data.user?.status === User.STATUSES.ACTIVE

      if (!emailConfirm) {
        this.sendActivationMail(data.user)

        store.commit('auth/setEmail', data.user.email)
        await router.push({name: "EmailSending"})
        return MessageService.errorMessage("Email не подтверждён")
      }

      await this.checkAuthState()
      await router.push({name: 'Home'})

      MessageService.successMessage("Вход выполнен")
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка входа")
    }
  }

  async loginByLink({code, password}) {
    try {
      const {data} = await $api.post(`/api/auth/invitation/complete`, { code, password })

      await this.checkAuthState()
      await router.push({name: 'Home'})

      MessageService.successMessage("Вход выполнен")
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка входа")
    }
  }

  async logout() {
    try {
      await $api.get('/api/auth/logout')

      await this.setAuthState(null)
      await router.push({name: 'Login'})

      MessageService.successMessage("Вы вышли из аккаунта")
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || "Ошибка")
    }
  }

  onAuthStateChange(callback) {
    this.onAuthStateChangeCb = callback
  }

  async checkAuthState() {
    try {
      const data = await this.refreshToken()

      if (data.user) {
        store.commit('currentUser/set', data.user)
      }
      await this.setAuthState(data)
    }catch (e) {
      await this.setAuthState(null)
    }
  }

  async setAuthState(state) {
    const user = state?.user
    if (!user || user.status !== User.STATUSES.ACTIVE) {
      tokenService.clearToken()
      store.commit('auth/setAuth', false)
    }else {
      tokenService.token = state.accessToken
      store.commit('auth/setAuth', true)
    }

    return await this.onAuthStateChangeCb.call(this, user ? new User(user) : user)
  }

  async refreshToken() {
    const {data} = await axios.get('/api/auth/refresh', {withCredentials: true})
    return data
  }

  async sendActivationMail({email}) {

  }

  async resetPassword({email}) {
    try {
      const {data} = await $api.post('/api/auth/reset', { email })
      MessageService.successMessage("На указанный email отправлена информация для восстановления")
    }catch (e) {
      MessageService.errorMessage(e?.response?.data?.message || e.message || "Ошибка входа")
    }
  }
}

export default new AuthService()