import $api from "@/services/ApiSetupService";

export default class OrgService {
  static async create(title, ownerId) {
    const {org} = await $api.post('/api/org/create', {title, ownerId})

    return org
  }

  static async bindUser(orgId, userId) {
    return await $api.post('/api/org/bind', {orgId, userId})
  }

  static async checkExisting(id) {
    const {data} = await $api.get(`/api/org/get/${id}`)

    if (!data || !data.id) throw new Error("Организации с таким ID не найдено")
    return data
  }
}