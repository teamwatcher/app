import {MessageService} from "@/services/MessageService";
import $api from "@/services/ApiSetupService";
import FileDownload from "js-file-download";

class FileService {
  async downloadFile(path, fileName) {
    try {
      const {data} = await $api.get(path, {responseType: "blob"})

      await FileDownload(data, fileName);
    }catch (e) {
      MessageService.errorMessage(e)
    }
  }

  async downloadImportTemplate() {
    const path = '/api/file/get/template/import'
    await this.downloadFile(path, 'Template Import Users.xlsx')
  }
}

export default new FileService()