import axios from "axios";
import tokenService from "@/services/TokenService";

const $api = axios.create({
  withCredentials: true
})

$api.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${tokenService.token}`
  return config
})

$api.interceptors.response.use(
  (config) => {return config},
  async (error) => {
  const originalRequest = error.config

  if (error.response.status === 401 && error.config && !originalRequest._isRetry) {
    originalRequest._isRetry = true
    try {
      const response = await axios.get('/api/auth/refresh', {withCredentials: true})

      tokenService.token = response.data.accessToken
      return $api.request(originalRequest)
    }catch (e) {
      // console.log('Не авторизован')
      throw error
    }
  }

  throw error
})

export default $api

