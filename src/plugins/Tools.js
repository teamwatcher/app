export default class Tools {
  static isEqual(object1, object2) {
    const props1 = Object.getOwnPropertyNames(object1);
    const props2 = Object.getOwnPropertyNames(object2);

    if (props1.length !== props2.length) {
      return false;
    }

    for (let i = 0; i < props1.length; i += 1) {
      const prop = props1[i];
      const bothAreObjects = typeof (object1[prop]) === 'object' && typeof (object2[prop]) === 'object';

      if ((!bothAreObjects && (object1[prop] !== object2[prop]))
        || (bothAreObjects && !isEqual(object1[prop], object2[prop]))) {
        return false;
      }
    }

    return true;
  }

  static getFlatNodes(nodes, childrenName='children') {
    let res = [...nodes]

    nodes.forEach(node => {
      if (node[childrenName] && node[childrenName].length) {
        res.push(...Tools.getFlatNodes(node[childrenName]))
      }
    })

    return res
  }

  static copyText(text) {
    return navigator.clipboard.writeText(text)
  }

  static getInnerValue(obj, key) {
    const keys = key.split("."); // Разделяем строку ключа по точке
    let value = obj;

    // Проходим по каждому ключу и получаем значение
    for (const k of keys) {
      if (value[k]) {
        value = value[k];
      } else {
        value = undefined;
        break;
      }
    }

    return value;
  }

  static copyObject(obj) {
    return JSON.parse(JSON.stringify(obj))
  }

  static getMonthLabel(months) {
    if (months % 10 === 1 && months % 100 !== 11) {
      return months + ' месяц';
    } else if (months % 10 >= 2 && months % 10 <= 4 && (months % 100 < 10 || months % 100 >= 20)) {
      return months + ' месяца';
    } else {
      return months + ' месяцев';
    }
  }

  static getYearLabel(years) {
    if (years % 10 === 1 && years % 100 !== 11) {
      return years + ' год';
    } else if (years % 10 >= 2 && years % 10 <= 4 && (years % 100 < 10 || years % 100 >= 20)) {
      return years + ' года';
    } else {
      return years + ' лет';
    }
  }
}