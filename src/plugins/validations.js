export default {
  req: v => !!v || 'Обязательное поле',
  email: (value) => {
    const pattern =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(value) || "Некорректный e-mail.";
  },
  min: (n) => (value) => (value && value.length >= +n) || `Минимум ${n} символов`,
  minN: (n) => (value) => (value !== undefined && value >= +n) || `Число не может быть меньше ${n}`,
  maxN: (n) => (value) => (value !== undefined && value <= +n) || `Число не может быть больше ${n}`,
  fio: v => !v || v.trim().split(' ').length > 2 || 'Введите ФИО полностью',
  passEqual: (val) => (v) => val === v || "Пароли не совпадают",
  isPositiveNumber: val => val > 0 || "Число должно быть больше 0"
}