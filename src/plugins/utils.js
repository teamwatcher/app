import {MessageService} from "@/services/MessageService";
const moment = require('moment')

export async function getHolidayList(year) {
  try {
      const api = require('isdayoff')()
      const startDate = `${year}-01-01`
      const endDate = `${year}-12-31`
      const stateDays = await api.period({
        start: new Date(startDate), end: new Date(endDate)
      })

      const  holidayList = []

      let day = moment(new Date(startDate))
      stateDays.map((state) => {
        if (state === 1 && day.weekday() !== 5 && day.weekday() !== 6) {
          holidayList.push(moment(day).format('YYYY-MM-DD'))
        }

        day.add(1, 'days')
      })

      return holidayList
    }catch (e) {
      MessageService.errorMessage("Не удалось загрузить даты. Попробуйте позднее")
      return []
    }
}
