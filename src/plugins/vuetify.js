import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import ru from 'vuetify/lib/locale/ru'

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: {ru},
    current: 'ru',
  },
  theme: {
    themes: {
      light: {
        primary: '#3f51b5',
        secondary: '#b0bec5',
        accent: '#8c9eff',
        error: '#bb1a1a',
        success: '#689F38',
        info: '#008daf',
        'bg-color': '#01579B'
      },
    },
  }
});
