const moment = require('moment')
import Tools from "@/plugins/Tools";

moment.prototype.toText = function () {
  return this.format('DD.MM.YYYY')
}
moment.prototype.toFileFormat = function (format = '"DD" piping YYYYг.') {
  //форматирвоание даты для заявления
  const months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',]
  const date = this
  const month = date.get('month')
  return date.format(format).replace('piping', months[month])
}

moment.prototype.getDateAndTime = function () {
  return this.toText() + " в " + this.format('HH:mm')
}

moment.prototype.weekDay = function (short = true) {
  const weekDayShort = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
  const weekDay = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
  const day = this.day()
  return short ? weekDayShort[day] : weekDay[day];
}

moment.prototype.getDurationLabel = function (months) {
  const y = Math.floor(months / 12)
  const m = months % 12

  const labels = []

  if (y > 0) labels.push( Tools.getYearLabel(y) )
  if (m > 0) labels.push( Tools.getMonthLabel(m) )

  return labels.join(' ')
}



Date.prototype.withoutTime = function () {
  const d = new Date(this);
  d.setHours(3, 0, 0, 0);
  return d;
}

Date.prototype.toText = function () {
  const d = this.getDate() < 10 ? '0' + this.getDate() : this.getDate()
  const mNumber = this.getMonth() + 1
  const m =  mNumber < 10 ? '0' + mNumber : mNumber
  return `${d}.${m}.${this.getFullYear()}`
}

Date.prototype.toTime = function () {
  const HH = this.getHours() < 10 ? '0' + this.getHours() : this.getHours()
  const MM = this.getMinutes() < 10 ? '0' + this.getMinutes() : this.getMinutes()
  return `${HH}:${MM}`
}

Date.prototype.toTextAndTime = function () {
  const date = this.toText()
  const time = this.toTime()

  return `${date} в ${time}`
}



Array.prototype.getRandomElement = function () {
  return this[Math.floor(Math.random()*this.length)]
}