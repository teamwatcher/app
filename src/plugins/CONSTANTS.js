export const USER_GROUPS = [
  {
    title: {single: 'Владелец Организации', multi: 'Владелецы Организации', short: 'СЕРВИС'},
    key: 990,
    color: 'deep-purple accent-4',
  },
  {
    title: {single: 'Админ Организации', multi: 'Админы Организации', short: 'СЕРВИС'},
    key: 950,
    color: 'deep-orange accent-2',
  },
  {
    title: {single: 'Генеральный директор', multi: 'Директора ', short: 'ГенДир'},
    key: 500,
    color: 'pink darken-3',
  },
  {
    title: {single: 'Директор', multi: 'Директора ', short: 'Директор'},
    key: 400,
    color: 'deep-purple accent-2',
  },
  {
    title: {single: 'Админ', multi: 'Администраторы', short: 'Админ'},
    key: 300,
    color: 'deep-orange accent-3',
  },
  {
    title: {single: 'Тимлидер', multi: 'Тимлидеры', short: 'Тимлидер'},
    key: 200,
    color: 'teal accent-4',
  },
  {
    title: {single: 'Сотрудник', multi: 'Сотрудники', short: 'Сотрудник'},
    key: 100,
    color: 'light-blue accent-3',
  },
]

export const ROLES = {
  SERVICE_ROOT: 990,
  SERVICE_ADMIN: 950,
  ORG_DIRECTOR: 500,
  DEP_DIRECTOR: 400,
  DEP_ADMIN: 300,
  DEP_TEAM_LEADER: 200,
  DEP_USER: 100,
}