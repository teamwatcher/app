import {Ability, AbilityBuilder} from "@casl/ability";
import {ROLES} from "@/plugins/CONSTANTS";
import Team from "@/models/Team";

const {can, cannot, rules} = new AbilityBuilder();

cannot('manage', 'all')
can('read', 'all')

const ability = new Ability(rules);

function defineAbilitiesFor(user, rootDepId) {
  const {can, cannot, rules} = new AbilityBuilder();
  cannot('manage', 'all')

  const roles = user.roles
  const depId = user.depId

  const ServiceRoot = roles.includes(ROLES.SERVICE_ROOT)
  const ServiceAdmin = roles.includes(ROLES.SERVICE_ADMIN)
  const Service = ServiceRoot || ServiceAdmin

  const OrgDirector = roles.includes(ROLES.ORG_DIRECTOR)
  const DepDirector = roles.includes(ROLES.DEP_DIRECTOR)
  const Director = OrgDirector || DepDirector

  const DepAdmin = roles.includes(ROLES.DEP_ADMIN)
  const DepLeader = roles.includes(ROLES.DEP_TEAM_LEADER)

  const structureObjects = ['Team', 'Task', 'Position']

  if (Service) {
    can('manage', 'all')

    cannot('setAsDepAdmin', 'User')
    if (!ServiceRoot) cannot('setAsServiceAdmin', 'User')
  }

  can('read', 'User')
  if (Director || DepAdmin) {
    if (DepDirector) can('setAsDepAdmin', 'User', {depId})

    can('manage', 'Structure')
    can('manage', 'Vacations')

    can('create', 'User')
    can('invite', 'User', {depId})
    can('update', 'User', {depId})
    can('delete', 'User', {depId})

    structureObjects.forEach(objName => {
      can('create', objName)
      can('update', objName, {depId})
      can('delete', objName, {depId})
    })

    can('manageVacation', 'User', {depId})
    can('approvingVacation', 'User', {depId})
  }else if (DepLeader) {
    can('manage', 'Structure')
    can('manage', 'Vacations')

    const teamId = Team.getTeamWhereLeader(user.id)
    const innerTeams = Team.getInnerTeamsById(teamId)
    const teams = [teamId, ...innerTeams]

    teams.forEach(id => {
      can('invite', 'User', {depId, 'job.team': id})
      can('update', 'User', {depId, 'job.team': id})
      can('delete', 'User', {depId, 'job.team': id})
      can('manageVacation', 'User', {depId, 'job.team': id})
      can('approvingVacation', 'User', {depId, 'job.team': id})
    })
  }


  can('manageVacation', 'User', {id: user.id})
  cannot('delete', 'Department', {id: rootDepId})
  cannot('delete', 'User', {id: user.id})
  ability.update(rules)
}

export {ability, defineAbilitiesFor};