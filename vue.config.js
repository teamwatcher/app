module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  // chainWebpack: config => {
  //   config
  //     .plugin('webpack-bundle-analyzer')
  //     .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
  // },
  publicPath: '/',
  devServer: {
    proxy: {
      '^/api': {
        // target: 'https://app.teamwatcher.ru', //web
        target: 'http://192.168.0.132:3000', //local
        ws: true,
        changeOrigin: true
      },
    }
  }
}